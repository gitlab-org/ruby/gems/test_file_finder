# frozen_string_literal: true

RSpec.describe TestFileFinder::FileFinder do
  let(:instance) { described_class.new(paths: paths) }
  let(:paths)    { [source_1, source_2] }
  let(:mapping)  { instance_double('TestFileFinder::Mapping') }
  let(:source_1) { 'models/widget.rb' }
  let(:source_2) { 'lib/something.rb' }
  let(:guess_1) { 'spec/models/widget.rb' }
  let(:guess_2) { 'spec/lib/something.rb' }
  let(:guess_3) { 'spec/controllers/widget_controller_spec.rb' }
  let(:guess_pattern_star) { 'spec/*/widget.rb' }
  let(:strategies) { [mapping] }

  describe '#test_files' do
    before do
      strategies.each { |strategy| instance.use(strategy) }

      allow(Dir).to receive(:glob).with(any_args).and_return([])
      allow(File).to receive(:exist?).with(any_args).and_return(false)

      allow(mapping).to receive(:match).with(paths).and_return([guess_1, guess_2, guess_3, guess_pattern_star])

      existing_paths.each do |file_path, mapped|
        if file_path.match?(/[*{\[?]/)
          allow(Dir).to receive(:glob).with(file_path).and_return([mapped])
        else
          allow(File).to receive(:exist?).with(file_path).and_return(true)
        end
      end
    end

    subject { instance.test_files }

    context 'when all guesses files exist' do
      let(:existing_paths) { [guess_1, guess_2, guess_3] }

      it { is_expected.to contain_exactly(guess_1, guess_2, guess_3) }
    end

    context 'when some file guesses exist' do
      let(:existing_paths) { [guess_1] }

      it { is_expected.to contain_exactly(guess_1) }
    end

    context 'when guesses are plain guesses and file name patterns' do
      let(:existing_paths) { { guess_2 => guess_2, guess_pattern_star => guess_1 } }

      it { is_expected.to contain_exactly(guess_2, guess_1) }
    end

    context 'when none of the file guesses exist' do
      let(:existing_paths) { [] }

      it { is_expected.to eq([]) }
    end

    context 'when mapping does not match a source to test' do
      let(:existing_paths) { [guess_1, guess_2, guess_3] }

      before do
        allow(mapping).to receive(:match).with(paths).and_return([])
      end

      it { is_expected.to eq([]) }
    end

    context 'when a test file is matched by multiple strategies' do
      let(:pattern_matching) { instance_double(TestFileFinder::MappingStrategies::PatternMatching) }
      let(:direct_matching) { instance_double(TestFileFinder::MappingStrategies::DirectMatching) }
      let(:instance) { described_class.new(paths: paths) }
      let(:existing_paths) { [guess_1, guess_2, guess_3] }
      let(:strategies) { [pattern_matching, direct_matching] }

      before do
        allow(pattern_matching).to receive(:match).with(paths).and_return([guess_1, guess_2])
        allow(direct_matching).to receive(:match).with(paths).and_return([guess_2, guess_3])
      end

      it { is_expected.to contain_exactly(guess_1, guess_2, guess_3) }
    end
  end
end
