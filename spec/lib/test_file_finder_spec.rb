# frozen_string_literal: true

RSpec.describe TestFileFinder do
  it 'has a version number' do
    expect(TestFileFinder::VERSION).not_to be nil
  end

  it 'loads the FileFinder' do
    expect { TestFileFinder::FileFinder }.not_to raise_error
  end
end
